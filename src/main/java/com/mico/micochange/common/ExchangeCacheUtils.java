package com.mico.micochange.common;

import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.huobi.HuobiExchange;

import java.util.HashMap;
import java.util.Map;

public class ExchangeCacheUtils {
    private static Map<String,Exchange> exchangeMap;


    public static Exchange getExchange(String name,String app_key,String app_secret){
        String key = name+app_key+app_secret;
        if(exchangeMap == null){
            exchangeMap = new HashMap<String, Exchange>();
        }
        if(exchangeMap.get(key)==null){
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(name, app_key, app_secret);
            exchangeMap.put(key,exchange);
        }
        return exchangeMap.get(key);
    }


    public static void putNewExchange(String name,String app_key,String app_secret){
        String key = name+app_key+app_secret;
        if(exchangeMap == null){
            exchangeMap = new HashMap<String, Exchange>();
        }
        exchangeMap.remove(key);
        if(exchangeMap.get(key)==null){
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(name, app_key, app_secret);
            exchangeMap.put(key,exchange);
        }
    }
}
