package com.mico.micochange.common;


import java.security.MessageDigest;

/**
 * MD5加密工具类
 * Created by wuyy on 2018/4/10.
 */
public class MD5Util {
	/**
	 * MD5计算
	 *
	 * @param  str 加密字符串
	 * @return 经过MD5加密之后的字符串
	 */
	public static String MD5Encode(String str) {
	    String resultStr = null;
	   try{
	       MessageDigest messageDigest=MessageDigest.getInstance("MD5");

	       messageDigest.reset();

	       messageDigest.update(str.getBytes("UTF-8"));
	       byte[] byteArray = messageDigest.digest();

	       StringBuffer md5StrBuff = new StringBuffer();

	       for (int i = 0; i < byteArray.length; i++) {
	           if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
	               md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
	           else
	               md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
	       }
	       resultStr = md5StrBuff.toString();
	   }catch(Exception e){
	       e.printStackTrace();
	   }
	    return resultStr;
	}
	public static void main(String[] args) {
		System.out.println(MD5Util.MD5Encode("12312345611"));
	}
	
	
}
