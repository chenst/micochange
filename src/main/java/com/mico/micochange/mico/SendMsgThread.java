package com.mico.micochange.mico;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.common.SmsSenderUtils;
import com.mico.micochange.server.MicoPublicThread;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.service.BinanceCancelOrderParams;
import com.mico.xchange.binance.service.BinanceTradeService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.service.BitstampTradeServiceRaw;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.ChaoexUtils;
import com.mico.xchange.chaoex.dto.ChaoexStringResult;
import com.mico.xchange.chaoex.dto.trade.ChaoexLogin;
import com.mico.xchange.chaoex.service.ChaoexTradeServiceRaw;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.service.HuobiTradeService;
import org.apache.mina.core.session.IoSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chenst on 2018/5/6.
 */
public class SendMsgThread extends MicoPublicThread{
    public SendMsgThread(IoSession session, String message) {
        super(session, message);
    }

    @Override
    public Map deal(JSONObject json) throws Exception {
        String msgType = json.getString("msgType");
        //策略短信发送
        if("steInf".equals(msgType)){
            //phoneNum,stfName,dealTime,coinType,dealPrice
            SmsSenderUtils.sendSteInf(json);
        }else if("verCode".equals(msgType)){
            //phoneNum,verCode
            SmsSenderUtils.sendVC(json.getString("phoneNum"),json.getString("verCode"));
        }
        Map retmap = new HashMap();
        retmap.put("result",true);
        return retmap;
    }
}
