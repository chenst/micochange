package com.mico.micochange.client;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.GZIPUtils;
import org.apache.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;


public class WebsocketClient {

    private static Logger logger = Logger.getLogger(WebsocketClient.class);
    public static WebSocketClient client;
    public static void main(String[] args) {
        try {
//            client = new WebSocketClient(new URI("ws://121.40.165.18:8800"),new Draft_6455()) {
            client = new WebSocketClient(new URI("wss://api.huobi.pro/ws"),new Draft_6455()) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    logger.info("握手成功");

                    if(client.isOpen()) {

                        Map<String, String> map = new HashMap<String, String>();
                        map.put("sub", "market.xrpbtc.kline.1min");
                        map.put("id", "xrpbtc");
                        client.send(JSONObject.toJSONString(map));
                        logger.info("信息发送成功");
                    }
                }

                @Override
                public void onMessage(String msg) {
                    logger.info("收到消息=========="+msg);
//                    msg = GZIPUtils.uncompressToString(msg.getBytes());
//                    logger.info("收到消息=========="+msg);
                    if(msg.equals("over")){
                        client.close();
                    }

                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    logger.info("链接已关闭");
                    logger.info("i: "+i+"; s: "+s+";b:"+b);
                    client.close();
                }

                @Override
                public void onError(Exception e){
                    e.printStackTrace();
                    logger.info("发生错误已关闭");
                }
            };
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        // WebSocket连接wss链接
        // This part is needed in case you are going to use self-signed
        // certificates
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };


        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        // Otherwise the line below is all that is needed.
        try {
            sc.init(null, null, null);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        /*client.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sc));
        client.set*/

        client.connect();
//        logger.info(client.getDraft());
        while(!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)){
//            logger.info("正在连接...");
        }
        //连接成功,发送信息
//        client.send(JSONObject.toJSONString("{\"req\": \"market.btcusdt.kline.1min\",\"id\": \"id10\"}"));
    }

}



