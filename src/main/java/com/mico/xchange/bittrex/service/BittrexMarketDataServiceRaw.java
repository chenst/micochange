package com.mico.xchange.bittrex.service;

import java.io.IOException;
import java.util.ArrayList;
import com.mico.xchange.Exchange;
import com.mico.xchange.bittrex.BittrexUtils;
import com.mico.xchange.bittrex.dto.marketdata.BittrexChartData;
import com.mico.xchange.bittrex.dto.marketdata.BittrexChartDataResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexCurrenciesResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexCurrency;
import com.mico.xchange.bittrex.dto.marketdata.BittrexDepth;
import com.mico.xchange.bittrex.dto.marketdata.BittrexDepthResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexMarketSummariesResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexMarketSummary;
import com.mico.xchange.bittrex.dto.marketdata.BittrexMarketSummaryResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexSymbol;
import com.mico.xchange.bittrex.dto.marketdata.BittrexSymbolsResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexTicker;
import com.mico.xchange.bittrex.dto.marketdata.BittrexTickerResponse;
import com.mico.xchange.bittrex.dto.marketdata.BittrexTrade;
import com.mico.xchange.bittrex.dto.marketdata.BittrexTradesResponse;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.exceptions.ExchangeException;

public class BittrexMarketDataServiceRaw extends BittrexBaseService {

  /**
   * Constructor
   *
   * @param exchange
   */
  public BittrexMarketDataServiceRaw(Exchange exchange) {

    super(exchange);
  }

  public BittrexCurrency[] getBittrexCurrencies() throws IOException {

    BittrexCurrenciesResponse response = bittrexAuthenticated.getCurrencies();

    if (response.isSuccess()) {
      return response.getCurrencies();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public BittrexTicker getBittrexTicker(CurrencyPair currencyPair) throws IOException {
    BittrexTickerResponse response =
        bittrexAuthenticated.getTicker(BittrexUtils.toPairString(currencyPair));

    if (response.getSuccess()) {
      return response.getTicker();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public ArrayList<BittrexSymbol> getBittrexSymbols() throws IOException {

    BittrexSymbolsResponse response = bittrexAuthenticated.getSymbols();

    if (response.isSuccess()) {
      return response.getSymbols();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public BittrexMarketSummary getBittrexMarketSummary(String pair) throws IOException {

    BittrexMarketSummaryResponse response = bittrexAuthenticated.getMarketSummary(pair);

    if (response.getSuccess()) {
      return response.getMarketSummary();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public ArrayList<BittrexMarketSummary> getBittrexMarketSummaries() throws IOException {

    BittrexMarketSummariesResponse response = bittrexAuthenticated.getMarketSummaries();

    if (response.isSuccess()) {
      return response.getMarketSummaries();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public BittrexDepth getBittrexOrderBook(String pair, int depth) throws IOException {

    BittrexDepthResponse response = bittrexAuthenticated.getBook(pair, "both", depth);

    if (response.getSuccess()) {

      BittrexDepth bittrexDepth = response.getDepth();
      return bittrexDepth;
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public BittrexTrade[] getBittrexTrades(String pair) throws IOException {

    BittrexTradesResponse response = bittrexAuthenticated.getTrades(pair);

    if (response.getSuccess()) {

      BittrexTrade[] bittrexTrades = response.getTrades();
      return bittrexTrades;
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public ArrayList<BittrexChartData> getBittrexChartData(
      CurrencyPair currencyPair, BittrexChartDataPeriodType periodType) throws IOException {

    BittrexChartDataResponse response =
        bittrexV2.getChartData(BittrexUtils.toPairString(currencyPair), periodType.getPeriod());

    if (response.getSuccess()) {
      return response.getChartData();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public ArrayList<BittrexChartData> getBittrexLatestTick(
      CurrencyPair currencyPair, BittrexChartDataPeriodType periodType, Long timeStamp)
      throws IOException {
    BittrexChartDataResponse response =
        bittrexV2.getLatestTick(
            BittrexUtils.toPairString(currencyPair), periodType.getPeriod(), timeStamp);
    if (response.getSuccess()) {
      return response.getChartData();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }
}
