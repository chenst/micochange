package com.mico.xchange.fcoin.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public final class FCoinBalance {

  private final String currency;
  private final BigDecimal available;
  private final BigDecimal frozen;
  private final BigDecimal balance;


  public FCoinBalance(
          @JsonProperty("id") String currency,
          @JsonProperty("symbol") BigDecimal available,
          @JsonProperty("type") BigDecimal frozen,
          @JsonProperty("side") BigDecimal balance){
   this.currency = currency;
   this.available = available;
   this.frozen = frozen;
   this.balance = balance;
  }

  public String getCurrency() {
    return currency;
  }

  public BigDecimal getAvailable() {
    return available;
  }

  public BigDecimal getFrozen() {
    return frozen;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  @Override
  public String toString() {
    return "FCoinBalance{" +
            "currency='" + currency + '\'' +
            ", available=" + available +
            ", frozen=" + frozen +
            ", balance=" + balance +
            '}';
  }
}
