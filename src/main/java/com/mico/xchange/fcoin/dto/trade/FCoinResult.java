package com.mico.xchange.fcoin.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FCoinResult<V> {

  private final int status;
  private final V data;
  private final String msg;

  public FCoinResult(
      @JsonProperty("status") int status,
      @JsonProperty("data") V data,
      @JsonProperty("msg") String msg) {
    this.status = status;
    this.data = data;
    this.msg = msg;
  }

  public int getStatus() {
    return status;
  }

  public boolean isSuccess(){
    return status == 0;
  }

  public V getData() {
    return data;
  }

  public String getMsg() {
    return msg;
  }

  @Override
  public String toString() {
    return "FCoinResult{" +
            "status=" + status +
            ", data=" + data +
            ", msg='" + msg + '\'' +
            '}';
  }
}
