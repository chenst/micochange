package com.mico.xchange.fcoin.dto.trade;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FCoinQueryOrderResult extends FCoinResult<FCoinOpenOrder>{

  @JsonCreator
  public FCoinQueryOrderResult(
      @JsonProperty("status") int status,
      @JsonProperty("data") FCoinOpenOrder data,
      @JsonProperty("msg") String msg) {
    super(status,data,msg);
  }

}
