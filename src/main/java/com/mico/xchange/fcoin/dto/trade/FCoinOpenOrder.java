package com.mico.xchange.fcoin.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.trade.ChaoexOrder;

import java.util.List;

public final class FCoinOpenOrder {

  private final String id;
  private final String symbol;
  private final String type;
  private final String side;
  private final String price;
  private final String amount;
  private final String state;
  private final String executed_value;
  private final String fill_fees;
  private final String filled_amount;
  private final String created_at;
  private final String source;


  public FCoinOpenOrder(
          @JsonProperty("id") String id,
          @JsonProperty("symbol") String symbol,
          @JsonProperty("type") String type,
          @JsonProperty("side") String side,
          @JsonProperty("price") String price,
          @JsonProperty("amount") String amount,
          @JsonProperty("state") String state,
          @JsonProperty("executed_value") String executed_value,
          @JsonProperty("fill_fees") String fill_fees,
          @JsonProperty("filled_amount") String filled_amount,
          @JsonProperty("created_at") String created_at,
          @JsonProperty("source") String source){
    this.id = id;
    this.symbol = symbol;
    this.type = type;
    this.side = side;
    this.price = price;
    this.amount = amount;
    this.state = state;
    this.executed_value = executed_value;
    this.fill_fees = fill_fees;
    this.filled_amount = filled_amount;
    this.created_at = created_at;
    this.source = source;
  }

  public String getId() {
    return id;
  }

  public String getSymbol() {
    return symbol;
  }

  public String getType() {
    return type;
  }

  public String getSide() {
    return side;
  }

  public String getPrice() {
    return price;
  }

  public String getAmount() {
    return amount;
  }

  public String getState() {
    return state;
  }

  public String getExecuted_value() {
    return executed_value;
  }

  public String getFill_fees() {
    return fill_fees;
  }

  public String getFilled_amount() {
    return filled_amount;
  }

  public String getCreated_at() {
    return created_at;
  }

  public String getSource() {
    return source;
  }

  @Override
  public String toString() {
    return "FCoinOpenOrder{" +
            "id='" + id + '\'' +
            ", symbol='" + symbol + '\'' +
            ", type='" + type + '\'' +
            ", side='" + side + '\'' +
            ", price='" + price + '\'' +
            ", amount='" + amount + '\'' +
            ", state='" + state + '\'' +
            ", executed_value='" + executed_value + '\'' +
            ", fill_fees='" + fill_fees + '\'' +
            ", filled_amount='" + filled_amount + '\'' +
            ", created_at='" + created_at + '\'' +
            ", source='" + source + '\'' +
            '}';
  }
}
