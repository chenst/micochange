package com.mico.xchange.fcoin;

import com.mico.xchange.BaseExchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.fcoin.service.FCoinTradeService;
import si.mazi.rescu.SynchronizedValueFactory;

import java.io.IOException;

public class FCoinExchange extends BaseExchange {

  @Override
  public void applySpecification(ExchangeSpecification exchangeSpecification) {

    super.applySpecification(exchangeSpecification);
  }

  @Override
  protected void initServices() {
    this.tradeService = new FCoinTradeService(this);
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {

    ExchangeSpecification exchangeSpecification =
        new ExchangeSpecification(this.getClass().getCanonicalName());
    exchangeSpecification.setSslUri("https://api.fcoin.com");
    exchangeSpecification.setHost("api.fcoin.com");
    exchangeSpecification.setExchangeName("FCoin");
    exchangeSpecification.setMetaDataJsonFileOverride(null);
    exchangeSpecification.setExchangeDescription(
        "FCoin is a globally oriented crypto-currency trading platform.");

    return exchangeSpecification;
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {

    // This exchange doesn't use a nonce for authentication
    return null;
  }

  @Override
  public void remoteInit() throws IOException, ExchangeException {

  }
}
