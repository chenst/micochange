package com.mico.xchange.fcoin.service;

import java.math.BigDecimal;
import java.util.List;

import com.mico.xchange.Exchange;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.fcoin.dto.trade.*;

public class FCoinTradeServiceRaw extends FCoinBaseService {

  public FCoinTradeServiceRaw(Exchange exchange) {
    super(exchange);
  }

  /**
   * 限价下单
   * @param symbol
   * @param amount
   * @param price
   * @param side
   * @return
   * @throws Exception
   */
  public String placeLimitOrder(String symbol, BigDecimal amount, BigDecimal price, FCoinSide side)
          throws Exception {
    FCoinOrder order = new FCoinOrder(symbol, side, FCoinType.limit, price, amount);
    FCoinOrderResult result =
            fcoin.placeOrder(apiKey, System.currentTimeMillis(), signatureCreator, order);
    if (result.getStatus() == 0) {
      return result.getData();
    } else {
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }


  /**
   * 市场价格下单
   * @param symbol
   * @param amount
   * @param side
   * @return
   * @throws Exception
   */
  public String placeMarketOrder(String symbol, BigDecimal amount,FCoinSide side)
          throws Exception {
    FCoinOrder order = new FCoinOrder(symbol, side, FCoinType.market, null, amount);
    FCoinOrderResult result =
            fcoin.placeOrder(apiKey, System.currentTimeMillis(), signatureCreator, order);
    if (result.getStatus() == 0) {
      return result.getData();
    } else {
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }

  /**
   * 查询单个订单状态
   * @param order_id
   * @return
   * @throws Exception
   */
  public FCoinOpenOrder getOrder(String order_id) throws Exception{
    FCoinQueryOrderResult result = fcoin.getOrder(apiKey,System.currentTimeMillis(),signatureCreator,order_id);
    if(result.isSuccess()){
      return result.getData();
    }else {
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }

  /**
   * 撤销订单
   * @param order_id
   * @return
   */
  public boolean cancelOrder(String order_id) {
    FCoinOrderResult result = fcoin.cancelOrder(apiKey,System.currentTimeMillis(),signatureCreator,order_id);
    if(result.getStatus() == 0){
      return result.getData().equals("true");
    }else {
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }

  /**
   * 获取账户余额
   * @return
   */
  public List<FCoinBalance> getBalance(){
    FCoinBalanceResult result = fcoin.getBalance(apiKey,System.currentTimeMillis(),signatureCreator);
    if(result.isSuccess()){
      return result.getData();
    }else {
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }

}
