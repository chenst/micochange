package com.mico.xchange.fcoin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.mico.xchange.fcoin.dto.marketdata.FCoinPairsResult;
import com.mico.xchange.fcoin.dto.trade.*;
import si.mazi.rescu.ParamsDigest;

@Path("/v2")
@Produces(MediaType.APPLICATION_JSON)
public interface FCoin {

  @POST
  @Path("orders")
  @Consumes(MediaType.APPLICATION_JSON)
  FCoinOrderResult placeOrder(
          @HeaderParam("FC-ACCESS-KEY") String apiKey,
          @HeaderParam("FC-ACCESS-TIMESTAMP") Long timestamp,
          @HeaderParam("FC-ACCESS-SIGNATURE") ParamsDigest paramsDigest,
          FCoinOrder order);

  @GET
  @Path("orders")
  @Consumes(MediaType.APPLICATION_JSON)
  FCoinOrderResult getOrders(
          @HeaderParam("FC-ACCESS-KEY") String apiKey,
          @HeaderParam("FC-ACCESS-TIMESTAMP") Long timestamp,
          @HeaderParam("FC-ACCESS-SIGNATURE") ParamsDigest paramsDigest,
          FCoinOrderQuery order);

  @GET
  @Path("orders/{order_id}")
  FCoinQueryOrderResult getOrder(
          @HeaderParam("FC-ACCESS-KEY") String apiKey,
          @HeaderParam("FC-ACCESS-TIMESTAMP") Long timestamp,
          @HeaderParam("FC-ACCESS-SIGNATURE") ParamsDigest paramsDigest,
          @PathParam("order_id") String order_id);

  @GET
  @Path("accounts/balance")
  FCoinBalanceResult getBalance(
          @HeaderParam("FC-ACCESS-KEY") String apiKey,
          @HeaderParam("FC-ACCESS-TIMESTAMP") Long timestamp,
          @HeaderParam("FC-ACCESS-SIGNATURE") ParamsDigest paramsDigest);


  @POST
  @Path("orders/{order_id}/submit-cancel")
  FCoinOrderResult cancelOrder(
          @HeaderParam("FC-ACCESS-KEY") String apiKey,
          @HeaderParam("FC-ACCESS-TIMESTAMP") Long timestamp,
          @HeaderParam("FC-ACCESS-SIGNATURE") ParamsDigest paramsDigest,
          @PathParam("order_id") String order_id);


  @GET
  @Path("public/symbols")
  FCoinPairsResult getSymbols();

}
