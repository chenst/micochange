package com.mico.xchange.bitstamp.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.bitstamp.BitstampAdapters;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.marketdata.OrderBook;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.marketdata.Trades;
import com.mico.xchange.service.marketdata.MarketDataService;

import java.io.IOException;

/** @author Matija Mazi */
public class BitstampMarketDataService extends BitstampMarketDataServiceRaw
    implements MarketDataService {

  public BitstampMarketDataService(Exchange exchange) {
    super(exchange);
  }

  @Override
  public Ticker getTicker(CurrencyPair currencyPair, Object... args) throws IOException {
    return BitstampAdapters.adaptTicker(getBitstampTicker(currencyPair), currencyPair);
  }

  @Override
  public OrderBook getOrderBook(CurrencyPair currencyPair, Object... args) throws IOException {
    return BitstampAdapters.adaptOrderBook(getBitstampOrderBook(currencyPair), currencyPair);
  }

  @Override
  public Trades getTrades(CurrencyPair currencyPair, Object... args) throws IOException {
    BitstampTime time = args.length > 0 ? (BitstampTime) args[0] : null;
    return BitstampAdapters.adaptTrades(getTransactions(currencyPair, time), currencyPair);
  }
}
