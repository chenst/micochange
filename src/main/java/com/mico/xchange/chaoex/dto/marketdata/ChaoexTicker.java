package com.mico.xchange.chaoex.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;

public final class ChaoexTicker {

  private final BigDecimal buy;
  private final BigDecimal high;
  private final BigDecimal last;
  private final BigDecimal low;
  private final BigDecimal sell;
  private final BigDecimal vol;
  private final long currencyId;
  private final long baseCurrencyId;

  public ChaoexTicker(
          @JsonProperty("buy") BigDecimal buy,
          @JsonProperty("high") BigDecimal high,
          @JsonProperty("last") BigDecimal last,
          @JsonProperty("low") BigDecimal low,
          @JsonProperty("sell") BigDecimal sell,
          @JsonProperty("vol") BigDecimal vol,
          @JsonProperty("currencyId") long currencyId,
          @JsonProperty("baseCurrencyId") long baseCurrencyId) {
    this.buy = buy;
    this.high = high;
    this.last = last;
    this.low = low;
    this.sell = sell;
    this.vol = vol;
    this.currencyId = currencyId;
    this.baseCurrencyId = baseCurrencyId;
  }

  public ChaoexTicker(String str) {
    this.buy = null;
    this.high = null;
    this.last = null;
    this.low = null;
    this.sell = null;
    this.vol = null;
    this.currencyId = 0;
    this.baseCurrencyId = 0;
  }


  public BigDecimal getBuy() {
    return buy;
  }

  public BigDecimal getHigh() {
    return high;
  }

  public BigDecimal getLast() {
    return last;
  }

  public BigDecimal getLow() {
    return low;
  }

  public BigDecimal getSell() {
    return sell;
  }

  public BigDecimal getVol() {
    return vol;
  }

  public long getCurrencyId() {
    return currencyId;
  }

  public long getBaseCurrencyId() {
    return baseCurrencyId;
  }

  @Override
  public String toString() {
    return "ChaoexTicker{" +
            "buy=" + buy +
            ", high=" + high +
            ", last=" + last +
            ", low=" + low +
            ", sell=" + sell +
            ", vol=" + vol +
            ", currencyId=" + currencyId +
            ", baseCurrencyId=" + baseCurrencyId +
            '}';
  }
}
