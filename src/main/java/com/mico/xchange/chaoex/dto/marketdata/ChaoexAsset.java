package com.mico.xchange.chaoex.dto.marketdata;

public class ChaoexAsset {

  private final String asset;

  public ChaoexAsset(String asset) {
    this.asset = asset;
  }

  public String getAsset() {
    return asset;
  }

  @Override
  public String toString() {
    return String.format("HuobiAsset [%s]", asset);
  }
}
