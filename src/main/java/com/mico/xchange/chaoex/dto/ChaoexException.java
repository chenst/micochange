package com.mico.xchange.chaoex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChaoexException extends RuntimeException {

  public final int code;
  public final String msg;

  public ChaoexException(@JsonProperty("code") int code, @JsonProperty("msg") String msg) {
    super(code + ": " + msg);
    this.code = code;
    this.msg = msg;
  }
}
