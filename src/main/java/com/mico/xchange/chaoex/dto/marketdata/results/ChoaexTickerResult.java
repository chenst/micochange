package com.mico.xchange.chaoex.dto.marketdata.results;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexTicker;
import com.mico.xchange.chaoex.dto.ChaoexResult;

import java.util.Date;

public class ChoaexTickerResult extends ChaoexResult<ChaoexTicker> {


  @JsonCreator
  public ChoaexTickerResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexTicker tick,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, tick);
  }

}
