package com.mico.xchange.chaoex.dto.marketdata.results;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexAssetPair;

public class ChaoexAssetPairsResult extends ChaoexResult<ChaoexAssetPair[]> {

  public ChaoexAssetPairsResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexAssetPair[] result,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, result);
  }
}
