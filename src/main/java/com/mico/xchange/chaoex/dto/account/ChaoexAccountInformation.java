package com.mico.xchange.chaoex.dto.account;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public final class ChaoexAccountInformation {

  public final BigDecimal allMoney;
  public final BigDecimal takeBtcMax;
  public final BigDecimal takeBtcNum;
  public List<ChaoexBalance> coinList;

  public ChaoexAccountInformation(
      @JsonProperty("allMoney") BigDecimal allMoney,
      @JsonProperty("takeBtcMax") BigDecimal takeBtcMax,
      @JsonProperty("takeBtcNum") BigDecimal takeBtcNum,
      @JsonProperty("coinList") List<ChaoexBalance> coinList) {
    this.allMoney = allMoney;
    this.takeBtcMax = takeBtcMax;
    this.takeBtcNum = takeBtcNum;
    this.coinList = coinList;
  }

  public BigDecimal getAllMoney() {
    return allMoney;
  }

  public BigDecimal getTakeBtcMax() {
    return takeBtcMax;
  }

  public BigDecimal getTakeBtcNum() {
    return takeBtcNum;
  }

  public List<ChaoexBalance> getCoinList() {
    return coinList;
  }

  public void setCoinList(List<ChaoexBalance> coinList) {
    this.coinList = coinList;
  }

  @Override
  public String toString() {
    return "ChaoexAccountInformation{" +
            "allMoney=" + allMoney +
            ", takeBtcMax=" + takeBtcMax +
            ", takeBtcNum=" + takeBtcNum +
            ", coinList=" + coinList +
            '}';
  }
}
