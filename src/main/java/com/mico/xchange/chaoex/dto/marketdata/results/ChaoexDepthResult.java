package com.mico.xchange.chaoex.dto.marketdata.results;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexDepth;
import com.mico.xchange.chaoex.dto.ChaoexResult;

import java.util.Date;

public class ChaoexDepthResult extends ChaoexResult<ChaoexDepth> {

  @JsonCreator
  public ChaoexDepthResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexDepth depth,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, depth);
  }

}
