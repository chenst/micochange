package com.mico.xchange.chaoex.dto.trade.results;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.trade.ChaoexOpenOrder;

public class ChaoexOpenOrdersResult extends ChaoexResult<ChaoexOpenOrder> {


  @JsonCreator
  public ChaoexOpenOrdersResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexOpenOrder order,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, order);
  }

}
