package com.mico.xchange.chaoex.dto.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.currency.Currency;

import java.math.BigDecimal;

public final class ChaoexBalance {

  private final BigDecimal amount;
  private final BigDecimal baseCurrencyId;
  private final BigDecimal btc_value;
  private final BigDecimal cashAmount;
  private final BigDecimal currencyId;
  private final String currencyName;
  private final String currencyNameEn;
  private final BigDecimal freezeAmount;
  private final String icoUrl;

  /*
  *amount	持仓数量	decimal
baseCurrencyId	基础币id	int
btc_value	大约折合多少BTC	int
cashAmount	现金价值	decimal
currencyId	交易币id	int
currencyName	数字货币全称	string
currencyNameEn	数字货币简称	string
freezeAmount	冻结数量	decimal
icoUrl	币种缩略图	string
   */
  public ChaoexBalance(
      @JsonProperty("amount") BigDecimal amount,
      @JsonProperty("baseCurrencyId") BigDecimal baseCurrencyId,
      @JsonProperty("btc_value") BigDecimal btc_value,
      @JsonProperty("cashAmount") BigDecimal cashAmount,
      @JsonProperty("currencyId") BigDecimal currencyId,
      @JsonProperty("currencyName") String currencyName,
      @JsonProperty("currencyNameEn") String currencyNameEn,
      @JsonProperty("freezeAmount") BigDecimal freezeAmount,
      @JsonProperty("icoUrl") String icoUrl) {

    this.amount = amount;
    this.baseCurrencyId =baseCurrencyId;
    this.btc_value = btc_value;
    this.cashAmount = cashAmount;
    this.currencyId = currencyId;
    this.currencyName = currencyName;
    this.currencyNameEn = currencyNameEn;
    this.freezeAmount = freezeAmount;
    this.icoUrl = icoUrl;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public BigDecimal getBaseCurrencyId() {
    return baseCurrencyId;
  }

  public BigDecimal getBtc_value() {
    return btc_value;
  }

  public BigDecimal getCashAmount() {
    return cashAmount;
  }

  public BigDecimal getCurrencyId() {
    return currencyId;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyNameEn() {
    return currencyNameEn;
  }

  public BigDecimal getFreezeAmount() {
    return freezeAmount;
  }

  public String getIcoUrl() {
    return icoUrl;
  }

  @Override
  public String toString() {
    return "ChaoexBalance{" +
            "amount=" + amount +
            ", baseCurrencyId=" + baseCurrencyId +
            ", btc_value=" + btc_value +
            ", cashAmount=" + cashAmount +
            ", currencyId=" + currencyId +
            ", currencyName='" + currencyName + '\'' +
            ", currencyNameEn='" + currencyNameEn + '\'' +
            ", freezeAmount=" + freezeAmount +
            ", icoUrl='" + icoUrl + '\'' +
            '}';
  }
}
