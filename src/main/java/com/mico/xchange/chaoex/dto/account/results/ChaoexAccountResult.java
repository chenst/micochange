package com.mico.xchange.chaoex.dto.account.results;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.account.ChaoexAccountInformation;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexTicker;

public class ChaoexAccountResult extends ChaoexResult<ChaoexAccountInformation> {


  @JsonCreator
  public ChaoexAccountResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexAccountInformation account,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, account);
  }

}
