package com.mico.xchange.chaoex.dto.marketdata.results;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexAsset;
import com.mico.xchange.chaoex.dto.ChaoexResult;

public class ChaoexAssetsResult extends ChaoexResult<ChaoexAsset[]> {

  public ChaoexAssetsResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexAsset[] result,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, result);
  }
}
