package com.mico.xchange.chaoex.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.BiConsumer;

public final class ChaoexDepth {
 //TODO chenst
  private final SortedMap<BigDecimal, BigDecimal> bids;
  private final SortedMap<BigDecimal, BigDecimal> asks;

  public ChaoexDepth(
      @JsonProperty("bids") List<Object[]> bidsJson,
      @JsonProperty("asks") List<Object[]> asksJson) {

      BiConsumer<Object[], Map<BigDecimal, BigDecimal>> entryProcessor =
              (obj, col) -> {
                  BigDecimal price = new BigDecimal(obj[0].toString());
                  BigDecimal qty = new BigDecimal(obj[1].toString());
                  col.put(price, qty);
              };
      TreeMap<BigDecimal, BigDecimal> bids = new TreeMap<>((k1, k2) -> -k1.compareTo(k2));
      TreeMap<BigDecimal, BigDecimal> asks = new TreeMap<>();

      bidsJson.stream().forEach(e -> entryProcessor.accept(e, bids));
      asksJson.stream().forEach(e -> entryProcessor.accept(e, asks));

      this.bids = Collections.unmodifiableSortedMap(bids);
      this.asks = Collections.unmodifiableSortedMap(asks);

  }

  public ChaoexDepth(String str){
        bids = null;
        asks = null;
  }


  public SortedMap<BigDecimal, BigDecimal> getBids() {
    return bids;
  }

  public SortedMap<BigDecimal, BigDecimal> getAsks() {
    return asks;
  }

  @Override
  public String toString() {
    return "HuobiDepth{" +
            "bids=" + bids +
            ", asks=" + asks +
            '}';
  }
}
