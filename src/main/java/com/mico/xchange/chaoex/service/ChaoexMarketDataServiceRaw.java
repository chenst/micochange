package com.mico.xchange.chaoex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.chaoex.ChaoexUtils;
import com.mico.xchange.chaoex.dto.marketdata.*;
import com.mico.xchange.chaoex.dto.marketdata.results.ChaoexAssetPairsResult;
import com.mico.xchange.chaoex.dto.marketdata.results.ChaoexDepthResult;
import com.mico.xchange.chaoex.dto.marketdata.results.ChoaexTickerResult;

import java.io.IOException;

public class ChaoexMarketDataServiceRaw extends ChaoexBaseService {

  protected ChaoexMarketDataServiceRaw(Exchange exchange) {
    super(exchange);
  }

/*
获取交易对信息
 */
  public ChaoexAssetPair[] getAssetPairs() throws IOException{

    ChaoexAssetPairsResult result =  chaoex.getAssetPairs();
    ChaoexAssetPair[] chaoexAssetPairs = checkResult(result);
    for(ChaoexAssetPair pair : chaoexAssetPairs){
      ChaoexUtils.putCurrency(pair.getBaseCurrency(),pair.getBaseCurrencyId());
      ChaoexUtils.putCurrency(pair.getQuoteCurrency(),pair.getTradeCurrencyId());
    }
    return chaoexAssetPairs;
  }

  /*
  获取行情
   */
  public ChaoexTicker getTicket(String baseCurrency,String tradeCurrency) throws IOException{
    String baseCurrencyId = ChaoexUtils.getCurrency(baseCurrency);
    String tradeCurrencyId = ChaoexUtils.getCurrency(tradeCurrency);
    ChoaexTickerResult result =  chaoex.getTicket(baseCurrencyId,tradeCurrencyId);
    return checkResult(result);
  }

/*
获取深度数据
 */
  public ChaoexDepth getDepth(String baseCurrency,String tradeCurrency,int limit) throws IOException{
    String baseCurrencyId = ChaoexUtils.getCurrency(baseCurrency);
    String tradeCurrencyId = ChaoexUtils.getCurrency(tradeCurrency);
    ChaoexDepthResult result =  chaoex.getDepth(baseCurrencyId,tradeCurrencyId,limit);
    return checkResult(result);
  }


}
