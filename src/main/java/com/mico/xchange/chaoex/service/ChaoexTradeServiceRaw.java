package com.mico.xchange.chaoex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.chaoex.dto.ChaoexException;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.ChaoexStringResult;
import com.mico.xchange.chaoex.dto.trade.*;
import com.mico.xchange.chaoex.dto.trade.results.ChaoexOpenOrdersResult;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChaoexTradeServiceRaw extends ChaoexBaseService {

  protected ChaoexTradeServiceRaw(Exchange exchange) {
    super(exchange);
  }



  public ChaoexStringResult newOrder(
      String buyOrSell,
      String baseCurrencyId,
      String tradeCurrencyId,
      BigDecimal quantity,
      BigDecimal price,
      String uid,
      String token)
      throws IOException, ChaoexException {
    return chaoex.newOrder(buyOrSell,baseCurrencyId,tradeCurrencyId,"",quantity,price,"5","1",token,uid,"zh_CN",getTimestamp(),
        super.signatureCreator);
  }


  public ChaoexOpenOrdersResult orderStatus(String startDate,String endDate,String baseId, String tradeId,
                                            String uid,
                                            String token)
      throws IOException, ChaoexException {
    return chaoex.orderStatus(startDate,endDate,1,999,10,0,baseId,tradeId,8,token,uid,"zh_CN",getTimestamp(),super.signatureCreator);
  }

  public ChaoexStringResult cancelOrder(
      String baseId,
      String orderId,
      String uid,
      String token)
      throws IOException, ChaoexException {
    return chaoex.cancelOrder(baseId,orderId,"",1,
            token,uid,"zh_CN",
        getTimestamp(),
        super.signatureCreator);
  }


}
