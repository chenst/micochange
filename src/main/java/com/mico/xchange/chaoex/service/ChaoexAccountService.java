package com.mico.xchange.chaoex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.chaoex.dto.account.ChaoexAccountInformation;
import com.mico.xchange.chaoex.dto.account.ChaoexAccountInformation;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.dto.account.Balance;
import com.mico.xchange.dto.account.FundingRecord;
import com.mico.xchange.dto.account.FundingRecord.Status;
import com.mico.xchange.dto.account.FundingRecord.Type;
import com.mico.xchange.dto.account.Wallet;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.exceptions.NotAvailableFromExchangeException;
import com.mico.xchange.exceptions.NotYetImplementedForExchangeException;
import com.mico.xchange.service.account.AccountService;
import com.mico.xchange.service.trade.params.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ChaoexAccountService extends ChaoexAccountServiceRaw implements AccountService {

  public ChaoexAccountService(Exchange exchange) {
    super(exchange);
  }

  /** (0:Email Sent,1:Cancelled 2:Awaiting Approval 3:Rejected 4:Processing 5:Failure 6Completed) */
  private static Status withdrawStatus(int status) {
    switch (status) {
      case 0:
      case 2:
      case 4:
        return Status.PROCESSING;
      case 1:
        return Status.CANCELLED;
      case 3:
      case 5:
        return Status.FAILED;
      case 6:
        return Status.COMPLETE;
      default:
        throw new RuntimeException("Unknown chaoex withdraw status: " + status);
    }
  }

  /** (0:pending,1:success) */
  private static Status depositStatus(int status) {
    switch (status) {
      case 0:
        return Status.PROCESSING;
      case 1:
        return Status.COMPLETE;
      default:
        throw new RuntimeException("Unknown chaoex deposit status: " + status);
    }
  }


}
