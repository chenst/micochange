package com.mico.xchange.chaoex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.dto.Order.IOrderFlags;
import com.mico.xchange.service.trade.TradeService;

public class ChaoexTradeService extends ChaoexTradeServiceRaw implements TradeService {

  public ChaoexTradeService(Exchange exchange) {
    super(exchange);
  }

  public interface ChaoexOrderFlags extends IOrderFlags {

    /** Used in fields 'newClientOrderId' */
    String getClientId();
  }


}
