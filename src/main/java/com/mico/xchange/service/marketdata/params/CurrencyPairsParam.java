package com.mico.xchange.service.marketdata.params;

import java.util.Collection;
import com.mico.xchange.currency.CurrencyPair;

public interface CurrencyPairsParam extends Params {

  Collection<CurrencyPair> getCurrencyPairs();
}
