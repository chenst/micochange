package com.mico.xchange.service.trade.params;

import com.mico.xchange.dto.Order.OrderType;

public interface CancelOrderByOrderTypeParams extends CancelOrderParams {
  OrderType getOrderType();
}
