package com.mico.xchange.service.trade.params;

import com.mico.xchange.currency.CurrencyPair;

public interface CancelOrderByCurrencyPair extends CancelOrderParams {

  public CurrencyPair getCurrencyPair();
}
