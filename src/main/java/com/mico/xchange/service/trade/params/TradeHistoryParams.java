package com.mico.xchange.service.trade.params;

import com.mico.xchange.service.trade.TradeService;

/**
 * Root interface for all interfaces used as a parameter type for {@link
 * TradeService#getTradeHistory(TradeHistoryParams)} .
 */
public interface TradeHistoryParams {}
