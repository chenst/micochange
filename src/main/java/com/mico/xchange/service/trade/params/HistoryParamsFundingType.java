package com.mico.xchange.service.trade.params;

import com.mico.xchange.dto.account.FundingRecord;

public interface HistoryParamsFundingType extends TradeHistoryParams {

  FundingRecord.Type getType();

  void setType(FundingRecord.Type type);
}
