package com.mico.xchange.service.trade.params;

import com.mico.xchange.currency.CurrencyPair;

public interface TradeHistoryParamCurrencyPair extends TradeHistoryParams {

  CurrencyPair getCurrencyPair();

  void setCurrencyPair(CurrencyPair pair);
}
