package com.mico.xchange.service.trade.params.orders;

import java.util.Collection;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.trade.LimitOrder;

public interface OpenOrdersParamMultiCurrencyPair extends OpenOrdersParams {
  @Override
  default boolean accept(LimitOrder order) {
    return order != null
        && getCurrencyPairs() != null
        && getCurrencyPairs().contains(order.getCurrencyPair());
  }

  Collection<CurrencyPair> getCurrencyPairs();

  void setCurrencyPairs(Collection<CurrencyPair> pairs);
}
