package com.mico.xchange.service.trade.params.orders;

import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.trade.LimitOrder;

public interface OpenOrdersParamCurrencyPair extends OpenOrdersParams {
  @Override
  default boolean accept(LimitOrder order) {
    return order != null
        && getCurrencyPair() != null
        && getCurrencyPair().equals(order.getCurrencyPair());
  }

  CurrencyPair getCurrencyPair();

  void setCurrencyPair(CurrencyPair pair);
}
