package com.mico.xchange.service.trade.params;

import com.mico.xchange.service.account.AccountService;

/**
 * Root interface for all interfaces used as a parameter type for {@link
 * AccountService#withdrawFunds(WithdrawFundsParams)} .
 */
public interface WithdrawFundsParams {}
