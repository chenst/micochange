package com.mico.xchange.service.trade.params;

import com.mico.xchange.currency.Currency;

public interface TradeHistoryParamCurrencies extends TradeHistoryParams {

  Currency[] getCurrencies();

  void setCurrencies(Currency[] currencies);
}
