package com.mico.xchange.okcoin.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.marketdata.OrderBook;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.marketdata.Trades;
import com.mico.xchange.okcoin.FuturesContract;
import com.mico.xchange.okcoin.OkCoinAdapters;
import com.mico.xchange.service.marketdata.MarketDataService;

import java.io.IOException;

public class OkCoinFuturesMarketDataService extends OkCoinMarketDataServiceRaw
    implements MarketDataService {
  /** Default contract to use */
  private final FuturesContract futuresContract;

  /**
   * Constructor
   *
   * @param exchange
   */
  public OkCoinFuturesMarketDataService(Exchange exchange, FuturesContract futuresContract) {

    super(exchange);

    this.futuresContract = futuresContract;
  }

  @Override
  public Ticker getTicker(CurrencyPair currencyPair, Object... args) throws IOException {
    if (args != null && args.length > 0) {
      return OkCoinAdapters.adaptTicker(
          getFuturesTicker(currencyPair, (FuturesContract) args[0]), currencyPair);
    } else {
      return OkCoinAdapters.adaptTicker(
          getFuturesTicker(currencyPair, futuresContract), currencyPair);
    }
  }

  @Override
  public OrderBook getOrderBook(CurrencyPair currencyPair, Object... args) throws IOException {
    if (args != null && args.length > 0) {
      return OkCoinAdapters.adaptOrderBook(
          getFuturesDepth(currencyPair, (FuturesContract) args[0]), currencyPair);
    } else {
      return OkCoinAdapters.adaptOrderBook(
          getFuturesDepth(currencyPair, futuresContract), currencyPair);
    }
  }

  @Override
  public Trades getTrades(CurrencyPair currencyPair, Object... args) throws IOException {
    if (args != null && args.length > 0) {
      return OkCoinAdapters.adaptTrades(
          getFuturesTrades(currencyPair, (FuturesContract) args[0]), currencyPair);
    } else {
      return OkCoinAdapters.adaptTrades(
          getFuturesTrades(currencyPair, futuresContract), currencyPair);
    }
  }
}
