package com.mico.xchange.okcoin.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class OkCoinExchangeRate {

  private final BigDecimal rate;

  public OkCoinExchangeRate(
      @JsonProperty("rate") final BigDecimal rate) {
      this.rate = rate;
  }

  public BigDecimal getRate() {
    return rate;
  }

  @Override
  public String toString() {
    return "OkCoinExchangeRate{" +
            "rate=" + rate +
            '}';
  }
}
