package com.mico.xchange.huobi;

import com.mico.xchange.BaseExchange;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.huobi.dto.marketdata.HuobiAsset;
import com.mico.xchange.huobi.dto.marketdata.HuobiAssetPair;
import com.mico.xchange.huobi.service.HuobiAccountService;
import com.mico.xchange.huobi.service.HuobiMarketDataService;
import com.mico.xchange.huobi.service.HuobiMarketDataServiceRaw;
import com.mico.xchange.huobi.service.HuobiTradeService;
import com.mico.xchange.utils.nonce.CurrentTimeNonceFactory;
import si.mazi.rescu.SynchronizedValueFactory;

import java.io.IOException;

public class HuobiHadaxExchange extends BaseExchange implements Exchange {

  private final SynchronizedValueFactory<Long> nonceFactory = new CurrentTimeNonceFactory();

  @Override
  protected void initServices() {
    this.marketDataService = new HuobiMarketDataService(this);
    this.tradeService = new HuobiTradeService(this);
    this.accountService = new HuobiAccountService(this);
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {
    ExchangeSpecification exchangeSpecification =
        new ExchangeSpecification(this.getClass().getCanonicalName());
    exchangeSpecification.setSslUri("https://api.hadax.com/");
    exchangeSpecification.setHost("api.hadax.com");
    exchangeSpecification.setPort(80);
    exchangeSpecification.setExchangeName("Huobi");
    exchangeSpecification.setExchangeDescription(
        "Huobi is a Chinese digital currency trading platform and exchange based in Beijing");
    return exchangeSpecification;
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {
    return nonceFactory;
  }

  @Override
  public void remoteInit() throws IOException, ExchangeException {
      HuobiAssetPair[] assetPairs =
              ((HuobiMarketDataServiceRaw) marketDataService).getHuobiAssetPairs();
    HuobiAsset[] assets = ((HuobiMarketDataServiceRaw) marketDataService).getHuobiAssets("");
      exchangeMetaData = HuobiAdapters.adaptToExchangeMetaData(assetPairs, assets);

      HuobiAssetPair[] hadaxassetPairs =
              ((HuobiMarketDataServiceRaw) marketDataService).getHuobiHadaxAssetPairs();
      HuobiAsset[] hadaxAssets = ((HuobiMarketDataServiceRaw) marketDataService).getHuobiAssets("hadax");
      exchangeMetaData = HuobiAdapters.adaptToExchangeMetaData(hadaxassetPairs, hadaxAssets);
  }
}
