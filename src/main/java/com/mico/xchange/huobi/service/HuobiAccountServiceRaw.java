package com.mico.xchange.huobi.service;

import java.io.IOException;
import com.mico.xchange.Exchange;
import com.mico.xchange.huobi.HuobiUtils;
import com.mico.xchange.huobi.dto.account.HuobiAccount;
import com.mico.xchange.huobi.dto.account.HuobiBalance;
import com.mico.xchange.huobi.dto.account.results.HuobiAccountResult;
import com.mico.xchange.huobi.dto.account.results.HuobiBalanceResult;

public class HuobiAccountServiceRaw extends HuobiBaseService {

  HuobiAccountServiceRaw(Exchange exchange) {
    super(exchange);
  }

  HuobiBalance getHuobiBalance(String accountID,String type) throws IOException {
    HuobiBalanceResult huobiBalanceResult = null;
    if("hadax".equals(type)){
      huobiBalanceResult = huobi.getHadaxBalance(
              accountID,
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    }else {
      huobiBalanceResult = huobi.getBalance(
              accountID,
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    }
    return checkResult(huobiBalanceResult);
  }

  public HuobiAccount[] getAccounts() throws IOException {
    HuobiAccountResult huobiAccountResult =
        huobi.getAccount(
            exchange.getExchangeSpecification().getApiKey(),
            HuobiDigest.HMAC_SHA_256,
            2,
            HuobiUtils.createUTCDate(exchange.getNonceFactory()),
            signatureCreator);
    return checkResult(huobiAccountResult);
  }
}
