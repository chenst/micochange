package com.mico.xchange.huobi.service;

import java.io.IOException;
import java.util.Collection;
import com.mico.xchange.Exchange;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.trade.*;
import com.mico.xchange.huobi.HuobiAdapters;
import com.mico.xchange.huobi.dto.trade.HuobiOrder;
import com.mico.xchange.service.trade.TradeService;
import com.mico.xchange.service.trade.params.CancelOrderByIdParams;
import com.mico.xchange.service.trade.params.CancelOrderParams;
import com.mico.xchange.service.trade.params.TradeHistoryParams;
import com.mico.xchange.service.trade.params.orders.OpenOrdersParams;

public class HuobiTradeService extends HuobiTradeServiceRaw implements TradeService {

  public HuobiTradeService(Exchange exchange) {
    super(exchange);
  }

  @Override
  public UserTrades getTradeHistory(TradeHistoryParams tradeHistoryParams) throws IOException {
    return null;
  }

  @Override
  public Collection<Order> getOrder(String... orderIds) throws IOException {
    return HuobiAdapters.adaptOrders(getHuobiOrder(orderIds));
  }

  @Override
  public OpenOrdersParams createOpenOrdersParams() {
    return null;
  }

  @Override
  public TradeHistoryParams createTradeHistoryParams() {
    return null;
  }

  @Override
  public boolean cancelOrder(String orderId) throws IOException {
    return cancelHuobiOrder(orderId).length() > 0;
  }

  @Override
  public boolean cancelOrder(CancelOrderParams cancelOrderParams) throws IOException {
    return cancelOrderParams instanceof CancelOrderByIdParams
        && cancelOrder(((CancelOrderByIdParams) cancelOrderParams).getOrderId());
  }

  @Override
  public String placeMarketOrder(MarketOrder marketOrder) throws IOException {
    return placeHuobiMarketOrder(marketOrder,"");
  }

  public String placeHadaxMarketOrder(MarketOrder marketOrder) throws IOException {
    return placeHuobiMarketOrder(marketOrder,"hadax");
  }

  @Override
  public OpenOrders getOpenOrders() throws IOException {
    return getOpenOrders(createOpenOrdersParams());
  }

  @Override
  public OpenOrders getOpenOrders(OpenOrdersParams openOrdersParams) throws IOException {
    HuobiOrder[] openOrders = getHuobiOpenOrders();
    return HuobiAdapters.adaptOpenOrders(openOrders);
  }

  @Override
  public String placeLimitOrder(LimitOrder limitOrder) throws IOException {
    return placeHuobiLimitOrder(limitOrder,"");
  }

  public String placeHadaxLimitOrder(LimitOrder limitOrder) throws IOException {
    return placeHuobiLimitOrder(limitOrder,"hadax");
  }

  @Override
  public String placeStopOrder(StopOrder stopOrder) throws IOException {
    return null;
  }
}
