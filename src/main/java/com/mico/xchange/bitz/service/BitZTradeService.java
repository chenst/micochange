package com.mico.xchange.bitz.service;

import java.io.IOException;
import java.util.Collection;
import com.mico.xchange.Exchange;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.trade.MarketOrder;
import com.mico.xchange.dto.trade.UserTrades;
import com.mico.xchange.exceptions.NotAvailableFromExchangeException;
import com.mico.xchange.service.trade.TradeService;
import com.mico.xchange.service.trade.params.TradeHistoryParams;

public class BitZTradeService extends BitZTradeServiceRaw implements TradeService {

  public BitZTradeService(Exchange exchange) {
    super(exchange);
  }

  @Override
  public TradeHistoryParams createTradeHistoryParams() {
    throw new NotAvailableFromExchangeException();
  }

  @Override
  public Collection<Order> getOrder(String... orderIds) throws IOException {
    throw new NotAvailableFromExchangeException();
  }

  @Override
  public String placeMarketOrder(MarketOrder marketOrder) throws IOException {
    throw new NotAvailableFromExchangeException();
  }

  @Override
  public UserTrades getTradeHistory(TradeHistoryParams params) throws IOException {
    throw new NotAvailableFromExchangeException();
  }
}
