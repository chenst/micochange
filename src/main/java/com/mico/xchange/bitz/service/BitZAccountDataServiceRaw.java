package com.mico.xchange.bitz.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.binance.dto.BinanceException;
import com.mico.xchange.binance.dto.account.BinanceAccountInformation;
import com.mico.xchange.bitz.dto.account.BitZAccountInformation;
import com.mico.xchange.bitz.dto.marketdata.BitZKline;
import com.mico.xchange.bitz.dto.marketdata.BitZOrders;
import com.mico.xchange.bitz.dto.marketdata.BitZTicker;
import com.mico.xchange.bitz.dto.marketdata.BitZTrades;
import com.mico.xchange.bitz.dto.marketdata.result.BitZTickerAllResult;
import com.mico.xchange.bitz.dto.trade.result.BitZBalanceResult;

import java.io.IOException;
import java.util.Random;

public class BitZAccountDataServiceRaw extends BitZBaseService {

  public BitZAccountDataServiceRaw(Exchange exchange) {
    super(exchange);
  }
  public BitZBalanceResult account(long timestamp)
          throws IOException {
    String nonce = getRandomStr();
    return bitZAuthenticated.balances(super.apiKey,timestamp+"" ,nonce,signatureCreator);
  }

  private static String getRandomStr(){
    StringBuffer str = new StringBuffer();
    for(int i=0;i<6;i++){
      str.append(new Random().nextInt(10));
    }
    return str.toString();
  }
}
