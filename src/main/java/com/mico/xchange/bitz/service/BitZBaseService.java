package com.mico.xchange.bitz.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.bitz.BitZAuthenticated;
import com.mico.xchange.service.BaseExchangeService;
import com.mico.xchange.service.BaseService;
import com.mico.xchange.bitz.BitZ;
import si.mazi.rescu.ParamsDigest;
import si.mazi.rescu.RestProxyFactory;

import java.io.IOException;
import java.util.Date;

public class BitZBaseService extends BaseExchangeService implements BaseService {

  protected BitZ bitz;

  protected BitZAuthenticated bitZAuthenticated;
  protected final String apiKey;
  protected final ParamsDigest signatureCreator;

  public BitZBaseService(Exchange exchange) {
    super(exchange);
    this.bitz =
            RestProxyFactory.createProxy(BitZ.class, exchange.getExchangeSpecification().getSslUri());
    this.bitZAuthenticated =
            RestProxyFactory.createProxy(BitZAuthenticated.class, exchange.getExchangeSpecification().getSslUri());
    this.apiKey = exchange.getExchangeSpecification().getApiKey();
    this.signatureCreator = BitZDigest.createInstance(exchange.getExchangeSpecification().getSecretKey());
  }

  public long getTimestamp() throws IOException {
//    Date systemTime = new Date(System.currentTimeMillis());
//    Date serverTime = new Date(systemTime.getTime());
    long time = System.currentTimeMillis() / 1000;
    return time;
  }
}
