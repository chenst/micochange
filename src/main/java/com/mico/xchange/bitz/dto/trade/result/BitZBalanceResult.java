package com.mico.xchange.bitz.dto.trade.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.bitz.dto.BitZResult;
import com.mico.xchange.bitz.dto.account.BitZBalance;
import com.mico.xchange.bitz.dto.trade.BitZOpenOrder;

public class BitZBalanceResult extends BitZResult<BitZBalance> {

  public BitZBalanceResult(
      @JsonProperty("code") int code,
      @JsonProperty("msg") String message,
      @JsonProperty("data") BitZBalance data) {
    super(code, message, data);
  }

  @Override
  public String toString() {
    return "BitZBalanceResult{}";
  }
}
