package com.mico.xchange.bitmex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.bitmex.dto.account.BitmexAccount;
import com.mico.xchange.bitmex.dto.account.BitmexWallet;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.dto.account.Balance;
import com.mico.xchange.dto.account.Wallet;
import com.mico.xchange.service.account.AccountService;

import java.io.IOException;
import java.math.BigDecimal;

public class BitmexAccountService extends BitmexAccountServiceRaw implements AccountService {

  /**
   * Constructor
   *
   * @param exchange
   */
  public BitmexAccountService(Exchange exchange) {

    super(exchange);
  }

  @Override
  public AccountInfo getAccountInfo() throws IOException {
    BitmexAccount account = super.getBitmexAccountInfo();
    BitmexWallet bitmexWallet = getBitmexWallet();
    String username = account.getUsername();
    BigDecimal amount = bitmexWallet.getAmount();
    BigDecimal amt = amount.divide(BigDecimal.valueOf(100_000_000L));
    Balance balance = new Balance(Currency.BTC, amt);
    Wallet wallet = new Wallet(Currency.BTC.getSymbol(), balance);
    AccountInfo accountInfo = new AccountInfo(username, wallet);
    return accountInfo;
  }

  @Override
  public String withdrawFunds(Currency currency, BigDecimal amount, String address)
      throws IOException {
    return withdrawFunds(currency.getCurrencyCode(), amount, address);
  }

  @Override
  public String requestDepositAddress(Currency currency, String... args) throws IOException {
    String currencyCode = currency.getCurrencyCode();

    // bitmex seems to use a lowercase 't' in XBT
    // can test this here - https://testnet.bitmex.com/api/explorer/#!/User/User_getDepositAddress
    // uppercase 'T' will return 'Unknown currency code'
    if (currencyCode.equals("XBT")) {
      currencyCode = "XBt";
    }
    return requestDepositAddress(currencyCode);
  }
}
