炒客网

GET

获取币种交易对
request_url：baseUrl + coin/allCurrencyRelations

获取实时行情
request_url：baseUrl + quote/realTime

获取交易深度
request_url：baseUrl + quote/tradeDeepin

POST

登录
request_url：baseUrl + user/signLogin

委托下单
request_url：baseUrl + order/order

委托撤单
request_url：baseUrl + order/cancel

账户资产查询
request_url：baseUrl + coin/customerCoinAccount

用户委托单查询
request_url：baseUrl + user/trOrderListByCustomer